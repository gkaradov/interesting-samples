-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<George Karadov>
-- Create date: <4/12/2018>
-- Description:	<Template for traceble store procedure>
-- =============================================

--One of the problems with debugging a store procedure is the inability to 
--figure out exactly where an error occur within the execution sequence of a 
--store procedure.  If there is only one select statement for example, this is not so bad, 
--but if there are multiple select, insert or other events, and error occurs in one of them, 
--the store procedure only returns a generic error code, indicating that something happened inside.

--To overcome this, the store procedure can be separated into logical blocks and each 
--block can be isolated with a try catch block, providing two functions.  One � catch an error 
--and report an error, associated the given error with the appropriate logical block.  
--Provide additional trace functionality, such as time measure for the given logical block.  
--The trace functionality can be tailored to the needs of the application.

--The reporting is done through a table variable declared in the beginning 
--of the store procedure, and selected at the very end.  The format of the reporting table 
--can be tailored to the needs of the application, but the best approach is to use 
--standardized format for all stored procedures and on the receiving end, a class can be implemented 
--in as a base class, which include the format nee3ded to receive that table.  
--This class can be used as foundation class for all classes that receive store 
--procedure call results.  In this fashion, all store procedures can have common 
--return policy that will be uniform through the whole application.



CREATE PROCEDURE [dbo].[SP_TryCatchTemplate]
@parameterOne int,
@parameterTwo int, 
@trace  bit  -- switch the time trace on and off depending on the desired result.
as

begin

	declare @logTable table(
			ProcedureName [varchar](256) NOT NULL,
			MessageType int,   --- for this example 1 - error message, 2 - trace massage
			ExecutionTimeInMiliseconds int,
			Tstamp datetimeoffset(7),
			MessageText [varchar](max) NOT NULL
		);

		declare @blockBegin time(7);
		declare @blockEnd time(7);

		-- First Block
		BEGIN TRY

			--only execute the begin time tace if the treace input parameter is on
			if(@trace = 1)
				Begin
					set @blockBegin = ( SELECT CURRENT_TIMESTAMP  );
				End

			-- Add the actual work of the logical block here
			Select @parameterOne


			--only execute the end time tace logic if the treace input parameter is on
			if(@trace = 1)
				Begin
					set @blockEnd = ( SELECT CURRENT_TIMESTAMP  );
					-- optional - add the time stamp colum if needed
					INSERT INTO @logTable (ProcedureName, MessageType, ExecutionTimeInMiliseconds,MessageText)
					SELECT 'SP_TryCatchTemplate', 2, DATEDIFF( mcs,  @blockBegin, @blockEnd), 'Trace time of Block One'
				End
		END TRY
		BEGIN CATCH
			if(@trace = 1)
				Begin
					set @blockEnd = ( SELECT CURRENT_TIMESTAMP  );
					-- optional - add the time stamp colum if needed
					INSERT INTO @logTable (ProcedureName, MessageType, ExecutionTimeInMiliseconds, Tstamp, MessageText)
					SELECT 'SP_TryCatchTemplate', 1, DATEDIFF( mcs,  @blockBegin, @blockEnd), SYSDATETIMEOFFSET(), 'Blockl One Error : ' + ERROR_MESSAGE()
				End
			else
				Begin
					INSERT INTO @logTable (ProcedureName, MessageType, Tstamp,MessageText)
					SELECT 'SP_TryCatchTemplate',1 , SYSDATETIMEOFFSET(), 'Blockl One Error : ' + ERROR_MESSAGE()
				End
		END CATCH;


		-- Second Block
		BEGIN TRY

			--only execute the begin time tace if the treace input parameter is on
			if(@trace = 1)
				Begin
					set @blockBegin = ( SELECT CURRENT_TIMESTAMP  );
				End

			-- Add the actual work of the logical block here
			Select @parameterTwo


			--only execute the end time tace logic if the treace input parameter is on
			if(@trace = 1)
				Begin
					set @blockEnd = ( SELECT CURRENT_TIMESTAMP  );
					-- optional - add the time stamp colum if needed
					INSERT INTO @logTable (ProcedureName, MessageType, ExecutionTimeInMiliseconds,MessageText)
					SELECT 'SP_TryCatchTemplate', 2, DATEDIFF( mcs,  @blockBegin, @blockEnd), 'Trace time of Block Two'
				End
		END TRY
		BEGIN CATCH
			if(@trace = 1)
				Begin
					set @blockEnd = ( SELECT CURRENT_TIMESTAMP  );
					-- optional - add the time stamp colum if needed
					INSERT INTO @logTable (ProcedureName, MessageType, ExecutionTimeInMiliseconds, Tstamp, MessageText)
					SELECT 'SP_TryCatchTemplate', 1, DATEDIFF( mcs,  @blockBegin, @blockEnd), SYSDATETIMEOFFSET(), 'Blockl Two Error : ' + ERROR_MESSAGE()
				End
			else
				Begin
					INSERT INTO @logTable (ProcedureName, MessageType, Tstamp,MessageText)
					SELECT 'SP_TryCatchTemplate',1 , SYSDATETIMEOFFSET(), 'Blockl Two Error : ' + ERROR_MESSAGE()
				End
		END CATCH;

		---- repeat


		-- Add all other functionality here, by repeating the block structure

		-- End.  This select will return the log table as a separate table in the resulting dataset. use as desired.
		Select * from @logTable;

end
GO


	